sawmill Module

Author:
Paul Chvostek
<paul+drupal@it.ca>
http://www.it.ca/~paul/

DESCRIPTION
-----------
Adds the required Javascript to the bottom of all your Drupal pages to
allow screen dimensions to be tracked by Sawmill (http://www.sawmill.net/)

DEPENDENCIES
------------
None

INSTALLATION
------------
See INSTALL.txt in this directory.

MORE INFORMATION
----------------
The wishlist includes the following:
 * Built-in "check" using cURL
 * Admin control of when tag is included (a la googleanalytics)
 * WebNibbler support (maybe)

If you'd like to add something to the wishlist, please contact the author!

